#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "numbers.h"

/* Vynasobi hodnotu ulozenu v blobe desiatkovou hodnotou 10 */
void mul10(unsigned char b256[], int len){
  char carry = 0;
  int i;
  for (i = 0; i < len; i++){
    unsigned int byte = b256[i];
    byte *= 10;
    byte += carry;
    b256[i] = byte % 256;
    carry = byte / 256;
  }
}

/* Pripocita char k hodnote v blobe */
void add(unsigned char a, unsigned char b256[], int len){
  char carry = a;
  int i;
  for (i = 0; i < len; i++){
    int byte = b256[i];
    byte += carry;
    b256[i] = byte % 256;
    carry = byte / 256;
  }
}

/* Prelozi cislo so zakladom 10 do blobu base256, ten musi byt predom alokovany */
void decToLongBlob(char* number, unsigned char* base256){
  int n = strlen(number);
  int i;
  for (i = 0; i < n; i++){
    base256[i] = 0;
  }

  int neg = 0;
  i = 0;
  if (number[0] == '-'){
    neg = 1;
    i++;
  }

  while (i < n){
    mul10(base256, n);
    add(number[i] - 48, base256, n);
    i++;
  }

  if (neg){
    for (i = 0; i < n; i++){
      base256[i] = ~base256[i];
    }
    add(1, base256, n);
  }
}

char xtod(char c) {
   if (c>='0' && c<='9') {
     return c-'0';
   }
   if (c>='A' && c<='F') {
     return c-'A'+10;
   }
   if (c>='a' && c<='f') {
     return c-'a'+10;
   }
   return 0;
}

/* Prelozi cislo so zakladom 16 do blobu base256, ten musi byt predom alokovany */
void hexToLongBlob(char* number, unsigned char* base256){
  int n = strlen(number);
  int i;
  for (i = 0; i < n; i++){
    base256[i] = 0;
  }

  int readPos = strlen(number) - 1; //citajme od konca
  int writePos = 0;
  while (readPos > 1) {//nechceme spracova 0x zo zaciatku hexa
    base256[writePos] = xtod(number[readPos]);
    readPos--;//nevadi ak zlezieme az k x z 0xhh na zaciatku, lebo pridame 0
    base256[writePos] |= (xtod(number[readPos])<<4);
    readPos--;
    writePos++;
  }

}

void shorten(unsigned char* base256, int n, unsigned char** newBase256, int* newLength){
  int pos = n - 1;
  
  while(pos > 0){
    char next = 255 * (base256[pos - 1] >> 7);
    char red = base256[pos] ^ next;
    if (red != 0){
      break;
    }
    pos--;
  }
  pos++;

  *newLength = pos;
  *newBase256 = malloc(pos);
  memcpy(*newBase256, base256, pos);
}

void decToBlob(char* number, unsigned char** base256, int* n){
  unsigned char longBase[strlen(number)];
  decToLongBlob(number, longBase);
  shorten(longBase, strlen(number), base256, n);
}

void hexToBlob(char* number, unsigned char** base256, int* n){
  unsigned char longBase[strlen(number)];
  hexToLongBlob(number, longBase);
  shorten(longBase, strlen(number), base256, n);
}

/*
int main()
{
  printf( "I am alive!  Beware.\n" );
  unsigned char string[256];
  int i;
  for (i = 0; i < 256; i++){
    string[i] = 0;
  }

  printf("please, enter the number: ");
  scanf("%s", string);

  unsigned char* result;
  int n;
  decToBlob(string, &result, &n);

  printf("short length: %d\n", n);
  printf("hex: ");
  for (i = n - 1; 0 <= i; i--){
    printf("%x%x ", result[i]/16, result[i]%16);
  }
  printf("\ndec: ");
  for (i = n - 1; 0 <= i; i--){
    printf("%d ", result[i]);
  }
  printf("\n");
  return 0;
}*/
