#ifndef TABLES_H
#define TABLES_H

typedef struct {
  char *name; //meno premennej ako ho zadal pouzivatel,
  //pouziva sa aj ako indikator, ci je struct inicializovany
  char *tname;  //prelozene meno
  int pos;
} _Variable;
typedef _Variable Variable;

typedef struct {
  char *name;
  int paramcount;
} _Function;
typedef _Function Function;

typedef struct {
  char *name;
  int paramcount[256];
  int paramcountSize;
} _Call;
typedef _Call Call;

void clearLocals();
// zmaze obsah tabulky lokalnych premennych
void clearGlobals();
// zmaze obsah tabulky globalnych premennych
void clearFunctions();
// zmaze obsah tabulky funkcii 

int varLocal(char *name);
// ak sa name nachadza v tabulke lokalnych premennych, tak vrati jemu
// prisluchajuce cislo, inak -1

char *varGlobal(char *name);
// ak sa name nachadza v tabulke globalnych premennych, tak vrati jemu
// prisluchajuce meno, inak NULL

char *varGlobalNo(int i);
// vrati prelozeny nazov i-tej globalnej premennej alebo null

int varFunction(char *name);
// ak sa name nachadza v tabulke funkcii, vrati pocet jej parametrov,
// inak vrati -1

int varFunctionCall(char *name);
//vrati kolko krat bola funkcia volana s roznymi poctami parametrov

void varInvalidFunctionCalls(char ***names, int *n);
//vrati pole nazvov funkcii, ktore boli nekorektne volane s roznymi
//poctami parametrov - mena neskopiruje, neslobodno ich menit

void addLocal(char *name);
// prida name do tabulky lokalnych premennych, zaroven k name asociuje
// cislo. Prve volanie addLocal asociuje cislo 0, dalsie 1, 2, ...
// Toto cislo je navratova hodnota funkcie varLocal v pripade, ze sa
// hladane meno zhoduje s menom v tabulke.
// Po volani clearLocals sa zacne asociovat opat od zaciatku, cislom 0
// Priklad:
//   addLocal("Ahoj");
//   varLocal("Ahoj");  //vrati 0
//   addLocal("Svet");
//   varLocal("Svet");  //vrati 1
//   clearLocals();
//   addLocal("Fero");
//   varLocal("Fero");  //vrati 0

void addGlobal(char *name);
// prida name do tabulky globalnych premennych, zaroven k nemu asociuje
// odlisne, unikatne meno. Toto meno sa vrati pri varGlobal s rovnakym
// parametrom name

void addFunction(char *name, int paramcount);
// prida funkciu name a jej prisluchajuci pocet parametrov do tabulky
// funkcii

void addFunctionCall(char *name, int paramcount);
// prida nazov funkcie a pocet argumentov do zoznamu volanych funkcii

#endif
