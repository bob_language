#ifndef NUMBERS_H
#define NUMBERS_H

/* Desiatkove cislo zakodovane v stringu number prelozi do blobu
 * prakticky to realizuje prevod do sustavy so zakladom 256
 *
 * number - string obsahujuci desiatkove cislo
 * base256 - pole, do ktoreho bol string prelozeny
 * n - pocet pouzitych bajtov pola.
 */
void decToBlob(char* number, unsigned char** base256, int* n);

/* Hexadecimalne cislo zakodovane v stringu number prelozi do blobu
 *
 * number - string tvaru 0xDDDD...
 * base256 - pole, do ktoreho bol string prelozeny
 * n - pocet pouzitych bajtov pola.
 */
void hexToBlob(char* number, unsigned char** base256, int* n);

#endif
