GCC=gcc

CFLAGS=-W -Wall -g3

all: BoB

lexer.c: lexer.lex
	flex -o lexer.c lexer.lex

lexer.o: lexer.c parser.h dynstrings.h core.h parser.h
	$(GCC) $(CFLAGS) -c -o lexer.o lexer.c

parser.h parser.c: parser.y
	bison -d -o parser.c parser.y

parser.o: parser.c parser.h lexer.o
	$(GCC) $(CFLAGS) -c -o parser.o parser.c

dynstrings.o: dynstrings.c dynstrings.h
	$(GCC) $(CFLAGS) -c -o dynstrings.o dynstrings.c

core.o: core.c core.h dynstrings.h tables.h core.h numbers.h assembly.h
	$(GCC) $(CFLAGS) -c -o core.o core.c

tables.o: tables.c tables.h
	$(GCC) $(CFLAGS) -c -o tables.o tables.c

numbers.o: numbers.c numbers.h
	$(GCC) $(CFLAGS) -c -o numbers.o numbers.c

assembly.c assembly.h: code.asm
	./preprocessor.sh

assembly.o: assembly.c assembly.h
	$(GCC) $(CFLAGS) -c -o assembly.o assembly.c

BoB: parser.o lexer.o dynstrings.o core.o tables.o numbers.o assembly.o
	$(GCC) $(CFLAGS)  parser.o lexer.o dynstrings.o core.o tables.o numbers.o assembly.o -lfl -lm -o BoB

clean: 
	rm *.o BoB parser.c parser.h lexer.c assembly.c assembly.h

