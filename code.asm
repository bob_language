jump BoBInit
  jump BBGetLength
  jump BBSetLength
  jump BBCopy
  jump BBGetElement
  jump BBSetElement
  jump BBNot
  jump BBAnd
  jump BBOr
  jump BBXor
  jump BBShiftLeft
  jump BBShiftRight
  jump BBNegate
  jump BBEquals
  jump BBGreater
  jump BBAdd
  jump BBSubtract
  jump BBMultiply
  jump BBDivide
  jump BBModulo
  jump BBgetchr
  jump BBgetint
  jump BBoutstr
  jump BFinalizeResult
  jump BBIsZero
  jump BBTrue
  jump BBFalse

    CMemCopy0Loop:                      @^0 = @^1 + 0
                                          ^0 = ^0 + 1
                                          ^1 = ^1 + 1
                                          ^2 = ^2 - 1
  CMemCopy:                             if ^2 CMemCopy0Loop
                                          pop
                                          pop
                                          pop
                                        return

    CMemSet0Loop:                       @^0 = ^2 + 0
                                          ^0 = ^0 + 1
                                          ^1 = ^1 - 1
  CMemSet:                              if ^1 CMemSet0Loop
                                          pop
                                          pop
                                          pop
                                        return

  ECommonException:                     push 42
    ECommonException0Loop:                  @^0 = 16 + 0
                                            ^0 = ^0 + 1
                                          jump ECommonException0Loop
                                          pop
                                        return

  ERangeCheckError:                     writechar 10
                                          writechar 82
                                          writechar 97
                                          writechar 110
                                          writechar 103
                                          writechar 101
                                          writechar 32
                                          writechar 67
                                          writechar 104
                                          writechar 101
                                          writechar 99
                                          writechar 107
                                          writechar 32
                                          writechar 69
                                          writechar 114
                                          writechar 114
                                          writechar 111
                                          writechar 114
                                          writechar 10
                                          writechar 10
                                          call ECommonException
                                        return

  EoutstrError:                         writechar 10
                                          writechar 111
                                          writechar 117
                                          writechar 116
                                          writechar 95
                                          writechar 115
                                          writechar 116
                                          writechar 114
                                          writechar 32
                                          writechar 69
                                          writechar 114
                                          writechar 114
                                          writechar 111
                                          writechar 114
                                          writechar 10
                                          writechar 10
                                          call ECommonException
                                        return

  EDivisionByZeroError:                 writechar 10
                                          writechar 68
                                          writechar 105
                                          writechar 118
                                          writechar 105
                                          writechar 115
                                          writechar 105
                                          writechar 111
                                          writechar 110
                                          writechar 32
                                          writechar 66
                                          writechar 121
                                          writechar 32
                                          writechar 90
                                          writechar 101
                                          writechar 114
                                          writechar 111
                                          writechar 32
                                          writechar 69
                                          writechar 114
                                          writechar 114
                                          writechar 111
                                          writechar 114
                                          writechar 10
                                          writechar 10
                                          call ECommonException
                                        return

  BCreateR:                             edx = eax + 2
                                          new ecx edx
                                          edx = ecx + 1
                                          @edx = eax + 0
                                          eax = ecx + 2
                                        return

  BBCreate:                             edx = ^0 + 2
                                          new ecx edx
                                          edx = ecx + 1
                                          @edx = ^0 + 0
                                          ^0 = ecx + 2
                                          top eax
                                        return

  BCreateEmptyR:                        push eax
                                          call BCreateR
                                          edx = eax + 0
                                          top ecx
                                          pop
    BCreateEmpty0Loop:                      @edx = 0 + 0
                                            edx = edx + 1
                                            ecx = ecx - 1
                                          if ecx BCreateEmpty0Loop
                                        return

  BDestroy:                             top eax
                                          pop
  BDestroyR:                              eax = eax - 2
                                          delete eax
                                        return

  BDestroyC:                            eax = ^0 - 2
                                          delete eax
                                        return

  BLengthC:                             eax = ^0 - 1
                                          eax = @eax + 0
                                        return

  BBGetLength:                          push ^0
                                          call BLengthC
                                          ^1 = eax + 0
                                          call BDestroy
                                          call BBToBlob
                                        return

  BBSetLength:                          push ^1
                                          call BToIndex
                                          ifeq eax ERangeCheckError
                                          push eax
                                          push ^1
                                          call BResize
                                          ecx = eax + 0
                                          eax = ^1 + 0
                                          ^1 = ecx + 0
                                          call BDestroyR
                                          call BDestroy
                                        return

  BResize:                              call BLengthC
                                          top ecx
                                          pop
                                          top edx
                                          edx = edx - eax
                                          if edx BResize0Bigger
                                            top eax
    BResize0Bigger:                       top edx
                                          pop
                                          push eax
                                          push ecx
                                          eax = edx + 0
                                          call BCreateEmptyR
                                          push eax
                                          call CMemCopy
                                        return

  BEnlarge:                             call BLengthC
                                          top ecx
                                          pop
                                          top edx
                                          pop
                                          push eax
                                          push ecx
                                          ecx = eax - edx
                                          if ecx BEnlarge0Bigger
                                            eax = edx + 0
    BEnlarge0Bigger:                      call BCreateEmptyR
                                          push eax
                                          call CMemCopy
                                        return

  BCopy:                                call BLengthC
                                          top edx
                                          pop
                                          push eax
                                          push edx
                                          call BCreateR
                                          push eax
                                          call CMemCopy
                                        return

  BBCopy:                               call BCopy
                                          push eax
                                        return

  BToIndex:                             call BOptimize
                                          push ebx
                                          push eax
                                          eax = eax - 1
                                          eax = @eax + 0
                                          ecx = eax - 4
                                          if ecx ERangeCheckError
                                          edx = ^0 + eax
                                          edx = edx - 1
                                          ecx = @edx & 128
                                          if ecx ERangeCheckError
                                          ebx = 0 + 0
    BToIndex0Loop:                          ebx = ebx << 8
                                            ebx = ebx + @edx
                                            edx = edx - 1
                                            eax = eax - 1
                                          if eax BToIndex0Loop
                                          call BDestroy
                                          eax = ebx + 0
                                          top ebx
                                          pop
                                        return

  BBToBlob:                             eax = 4 + 0
                                          call BCreateR
                                          edx = eax + 1
                                          @eax = ^0 & 255
                                          ^0 = ^0 >> 8
                                          @edx = ^0 & 255
                                          ^0 = ^0 >> 8
                                          edx = edx + 1
                                          @edx = ^0 & 255
                                          ^0 = ^0 >> 8
                                          edx = edx + 1
                                          @edx = ^0 & 255
                                          push eax
                                          push eax
                                          call BOptimize
                                          ^1 = eax + 0
                                          call BDestroy
                                        return

  BToPtr:                               top edx
                                          pop
                                          top eax
                                          pop
                                          push ebx
                                          ebx = edx + 0
                                          push eax
                                          call BToIndex
                                          push eax
                                          eax = ebx - 1
                                          eax = @eax + 0
                                          top ecx
                                          eax = ecx - eax
                                          ifgeq eax ERangeCheckError
                                          eax = ebx + ecx
                                          pop
                                          top ebx
                                          pop
                                        return

  BGetElement:                          call BToPtr
                                          push @eax
                                          eax = 1 + 0
                                          call BCreateR
                                          top @eax
                                          pop
                                        return

  BBGetElement:                         push 0
                                          push ^2
                                          push ^2
                                          call BGetElement
                                          ^0 = eax + 0
                                          eax = ^1 + 0
                                          call BDestroyR
                                          eax = ^2 + 0
                                          call BDestroyR
                                          ^2 = ^0 + 0
                                          pop
                                          pop
                                        return

  BBSetElement:                         push ^1
                                          call BToIndex
                                          ecx = eax + 0
                                          eax = ^1 + 0
                                          ^1 = ecx + 0
                                          call BDestroyR
                                          eax = ^2 - 1
                                          eax = @eax + 0
                                          push eax
                                          push ^3
                                          eax = eax + ^3
                                          push eax
                                          push ^3
                                          call BEnlarge
                                          edx = eax + ^3
                                          push edx
                                          call CMemCopy
                                          ^1 = ^2 + 0
                                          ^2 = eax + 0
                                          call BDestroy
                                          call BDestroy
                                        return

  BOptimize:                            call BLengthC
                                          top ecx
                                          pop
                                          eax = eax - 1
                                          edx = ecx + eax
                                          ifeq @edx BOptimize0RemoveZerosLoop
                                          edx = @edx - 255
                                          ifeq edx BOptimize0RemoveOnesLoop
                                          push ecx
                                          call BCopy
                                        return
    BOptimize0RemoveZerosLoop:            ifeq eax BOptimize0End
                                            edx = edx - 1
                                            edx = @edx & 128
                                            if edx BOptimize0End
                                            eax = eax - 1
                                            edx = ecx + eax
                                            ifeq @edx BOptimize0RemoveZerosLoop                                            
                                            jump BOptimize0End
    BOptimize0RemoveOnesLoop:             ifeq eax BOptimize0End
                                            edx = ecx + eax
                                            edx = edx - 1
                                            edx = @edx & 128
                                            ifeq edx BOptimize0End
                                            eax = eax - 1
                                            edx = ecx + eax
                                            edx = @edx - 255
                                            ifeq edx BOptimize0RemoveOnesLoop
    BOptimize0End:                        eax = eax + 1
                                          push eax
                                          push ecx
                                          call BResize
                                        return

  BNot:                                 push ebx
                                          eax = ^1 - 1
                                          eax = @eax + 0
                                          ebx = eax + 0
                                          call BCreateR
                                          edx = ^1 + 0
                                          ecx = eax + 0
    BNot0Loop:                              @ecx = @edx ^ 255
                                            ecx = ecx + 1
                                            edx = edx + 1
                                            ebx = ebx - 1
                                          if ebx BNot0Loop
                                          top ebx
                                          pop
                                          pop
                                        return

  BBNot:                                push ^0
                                          call BNot
                                          ecx = eax + 0
                                          eax = ^0 + 0
                                          ^0 = ecx + 0
                                          call BDestroyR
                                        return

  BAnd:                                 push ebx
                                          push esi
                                          eax = ^2 - 1
                                          eax = @eax + 0
                                          ebx = eax + 0
                                          eax = ^3 - 1
                                          eax = @eax + 0
                                          edx = eax - ebx
                                          if edx BAnd0Max
                                            edx = 0 - edx
                                            ecx = eax + 0
                                            eax = ebx + 0
                                            ebx = ecx + 0
                                            ecx = ^3 + 0
                                            ^3 = ^2 + 0
                                            ^2 = ecx + 0
    BAnd0Max:                             push 0
                                          push edx
                                          call BCreateR
                                          ecx = eax + 0
                                          esi = ^4 + 0
                                          edx = ^5 + 0
    BAnd0Loop:                              @ecx = @esi & @edx
                                            ecx = ecx + 1
                                            esi = esi + 1
                                            edx = edx + 1
                                            ebx = ebx - 1
                                          if ebx BAnd0Loop
                                          push ecx
                                          call CMemSet
                                          top esi
                                          pop
                                          top edi
                                          pop
                                          pop
                                          pop
                                        return

  BBAnd:                                push 0
                                          push ^2
                                          push ^2
                                          call BAnd
                                          ^0 = eax + 0
                                          eax = ^1 + 0
                                          call BDestroyR
                                          eax = ^2 + 0
                                          call BDestroyR
                                          ^2 = ^0 + 0
                                          pop
                                          pop
                                        return

  BOr:                                  push ebx
                                          push esi
                                          eax = ^2 - 1
                                          eax = @eax + 0
                                          ebx = eax + 0
                                          eax = ^3 - 1
                                          eax = @eax + 0
                                          edx = eax - ebx
                                          if edx BOr0Max
                                            edx = 0 - edx
                                            ecx = eax + 0
                                            eax = ebx + 0
                                            ebx = ecx + 0
                                            ecx = ^3 + 0
                                            ^3 = ^2 + 0
                                            ^2 = ecx + 0
    BOr0Max:                              push edx
                                          call BCreateR
                                          ecx = eax + 0
                                          esi = ^3 + 0
                                          edx = ^4 + 0
    BOr0Loop:                               @ecx = @esi | @edx
                                            ecx = ecx + 1
                                            esi = esi + 1
                                            edx = edx + 1
                                            ebx = ebx - 1
                                          if ebx BOr0Loop
                                          push edx
                                          push ecx
                                          call CMemCopy
                                          top esi
                                          pop
                                          top edi
                                          pop
                                          pop
                                          pop
                                        return

  BBOr:                                 push 0
                                          push ^2
                                          push ^2
                                          call BOr
                                          ^0 = eax + 0
                                          eax = ^1 + 0
                                          call BDestroyR
                                          eax = ^2 + 0
                                          call BDestroyR
                                          ^2 = ^0 + 0
                                          pop
                                          pop
                                        return

  BXor:                                 push ebx
                                          push esi
                                          eax = ^2 - 1
                                          eax = @eax + 0
                                          ebx = eax + 0
                                          eax = ^3 - 1
                                          eax = @eax + 0
                                          edx = eax - ebx
                                          if edx BXor0Max
                                            edx = 0 - edx
                                            ecx = eax + 0
                                            eax = ebx + 0
                                            ebx = ecx + 0
                                            ecx = ^3 + 0
                                            ^3 = ^2 + 0
                                            ^2 = ecx + 0
    BXor0Max:                             push edx
                                          call BCreateR
                                          ecx = eax + 0
                                          esi = ^3 + 0
                                          edx = ^4 + 0
    BXor0Loop:                              @ecx = @esi ^ @edx
                                            ecx = ecx + 1
                                            esi = esi + 1
                                            edx = edx + 1
                                            ebx = ebx - 1
                                          if ebx BXor0Loop
                                          push edx
                                          push ecx
                                          call CMemCopy
                                          top esi
                                          pop
                                          top edi
                                          pop
                                          pop
                                          pop
                                        return

  BBXor:                                push 0
                                          push ^2
                                          push ^2
                                          call BXor
                                          ^0 = eax + 0
                                          eax = ^1 + 0
                                          call BDestroyR
                                          eax = ^2 + 0
                                          call BDestroyR
                                          ^2 = ^0 + 0
                                          pop
                                          pop
                                        return

  BToShiftLow:                          call BLengthC
                                          top ecx
                                          pop
                                          edx = 0 + 0
                                          ifleq eax BToShiftLow0End
                                            edx = @ecx & 7
    BToShiftLow0End:                      eax = edx + 0
                                        return

  BToShiftHigh:                         call BLengthC
                                          edx = eax - 5
                                          ifl edx BToShiftHigh0Small
                                            eax = 5 + 0
                                            ecx = ^0 + 4
                                            ecx = @ecx >> 2
                                            if ecx BToShiftHigh0Overflow
                                            ecx = ^0 + 5
                                            jump BToShiftHigh0Check
    BToShiftHigh0Loop:                        if @ecx BToShiftHigh0Overflow
                                              ecx = ecx + 1
                                              edx = edx - 1
    BToShiftHigh0Check:                     if edx BToShiftHigh0Loop
    BToShiftHigh0Small:                   top ecx
                                          ^0 = @ecx >> 3
                                          push 5
                                          jump BToShiftHigh1Check
    BToShiftHigh1Loop:                      ecx = ecx + 1
                                            edx = @ecx << ^0
                                            ^1 = ^1 | edx
                                            ^0 = ^0 + 8
    BToShiftHigh1Check:                     eax = eax - 1
                                          if eax BToShiftHigh1Loop
                                          pop
                                          top eax
                                          pop
                                        return
    BToShiftHigh0Overflow:                eax = 2147483647 + 0
                                          pop
                                        return

  BShiftLeft:                           push ebx
                                          push esi
                                          push edi
                                          push ^4
                                          call BSign
                                          ifeq eax BShiftLeft0Positive
                                            push ^4
                                            call BNegate
                                            ebx = eax + 0
                                            push eax
                                            push ^4
                                            call BShiftRight
                                            esi = eax + 0
                                            eax = ebx + 0
                                            call BDestroyR
                                            eax = esi + 0
                                          jump BShiftLeft0End
    BShiftLeft0Positive:                    eax = ^3 - 1
                                            eax = @eax + 0
                                            push eax
                                            push ^5
                                            call BToShiftLow
                                            ebx = eax + 0
                                            push ^5
                                            call BToShiftHigh
                                            esi = ^0 + eax
                                            edi = ^4 + ^0
                                            edi = edi - 1
                                            edi = @edi << ebx
                                            edi = edi >> 8
                                            ifeq edi BShiftLeft0NoExtraNeeded
                                              eax = esi + 1
                                              call BCreateEmptyR
                                              esi = esi + eax
                                              @esi = edi + 0
                                            jump BShiftLeft0ExtraDone
    BShiftLeft0NoExtraNeeded:                 eax = esi + 0
                                              call BCreateEmptyR
                                              esi = esi + eax
    BShiftLeft0ExtraDone:                   esi = esi - ^0
                                            edx = 0 + 0
                                            edi = ^4 + 0
    BShiftLeft0Loop:                          ecx = @edi << ebx
                                              edx = edx | ecx
                                              @esi = edx & 255
                                              edx = edx >> 8
                                              edi = edi + 1
                                              esi = esi + 1
                                              ^0 = ^0 - 1
                                            if ^0 BShiftLeft0Loop
    BShiftLeft0End:                       pop
                                          top edi
                                          pop
                                          top esi
                                          pop
                                          top ebx
                                          pop
                                          pop
                                          pop
                                        return

  BBShiftLeft:                          push 0
                                          push ^2
                                          push ^2
                                          call BShiftLeft
                                          ^0 = eax + 0
                                          eax = ^1 + 0
                                          call BDestroyR
                                          eax = ^2 + 0
                                          call BDestroyR
                                          ^2 = ^0 + 0
                                          pop
                                          pop
                                        return

  BShiftRight:                          push ^1
                                          call BSign
                                          ifeq eax BShiftRight0Positive
                                            push ^1
                                            call BNegate
                                            ^1 = eax + 0
                                            push eax
                                            push ^1
                                            call BShiftLeft
                                            ^0 = eax + 0
                                            eax = ^1 + 0
                                            call BDestroyR
                                            top eax
                                            pop
                                            pop
                                        return
    BShiftRight0Positive:                 call BLengthC
                                          push eax
                                          push ^2
                                          call BToShiftHigh
                                          eax = ^0 - eax
                                          push eax
                                          push ^3
                                          call BToShiftLow
                                          ^3 = eax + 0
                                          push ebx
                                          ebx = ^2 + ^3
                                          ebx = ebx - 1
                                          eax = @ebx >> ^4
                                          ^3 = 0 + 0
                                          if eax BShiftRight0NoExtraNeeded
                                            ^1 = ^1 - 1
                                            ^3 = @ebx + 0
                                            ebx = ebx - 1
    BShiftRight0NoExtraNeeded:            if ^1 BShiftRight0NonZero
                                            eax = 1 + 0
                                            call BCreateEmptyR
                                          jump BShiftRight0End
    BShiftRight0NonZero:                    eax = ^1 + 0
                                            call BCreateR
                                            edx = eax + ^1
    BShiftRight0Loop:                         edx = edx - 1
                                              ^3 = ^3 << 8
                                              ^3 = ^3 | @ebx
                                              ebx = ebx - 1
                                              ecx = ^3 >> ^4
                                              @edx = ecx & 255
                                              ^1 = ^1 - 1
                                            if ^1 BShiftRight0Loop
    BShiftRight0End:                      top ebx
                                          pop
                                          pop
                                          pop
                                          pop
                                          pop
                                        return

  BBShiftRight:                         push 0
                                          push ^2
                                          push ^2
                                          call BShiftRight
                                          ^0 = eax + 0
                                          eax = ^1 + 0
                                          call BDestroyR
                                          eax = ^2 + 0
                                          call BDestroyR
                                          ^2 = ^0 + 0
                                          pop
                                          pop
                                        return

  BNegate:                              top eax
                                          pop
                                          push ebx
                                          push esi
                                          push edi
                                          ebx = eax + 0
                                          eax = eax - 1
                                          eax = @eax + 0
                                          esi = eax + 0
                                          call BCreateR
                                          edi = eax + 0
                                          ecx = 0 + 1
                                          jump BNegate0MiddleOfLoop
    BNegate0Loop:                           eax = eax + 1
                                            ebx = ebx + 1
    BNegate0MiddleOfLoop:                   edx = @ebx ^ 255
                                            edx = edx + ecx
                                            @eax = edx & 255
                                            ecx = edx >> 8
                                            esi = esi - 1
                                          if esi BNegate0Loop
                                          if ecx BNegate0OptimizeEnd
                                          edx = @eax ^ @ebx
                                          edx = edx >> 7
                                          if edx BNegate0OptimizeEnd
                                            eax = edi - 1
                                            eax = @eax + 0
                                            eax = eax + 1
                                            push eax
                                            push edi
                                            call BEnlarge
                                            jump BNegate0OptimizeCommon
    BNegate0OptimizeEnd:                  push edi
                                          call BOptimize
    BNegate0OptimizeCommon:                 esi = eax + 0
                                            eax = edi + 0
                                            call BDestroyR
                                            eax = esi + 0
                                          top edi
                                          pop
                                          top esi
                                          pop
                                          top ebx
                                          pop
                                        return

  BBNegate:                             push ^0
                                          call BNegate
                                          ecx = eax + 0
                                          eax = ^0 + 0
                                          ^0 = ecx + 0
                                          call BDestroyR
                                        return

  BSign:                                call BLengthC
                                          eax = eax + ^0
                                          eax = eax - 1
                                          eax = @eax >> 7
                                          pop
                                        return

  BSignExtend:                          call BLengthC
                                          push eax
                                          push eax
                                          push ^2
                                          eax = ^4 + 0
                                          call BCreateR
                                          push eax
                                          ^4 = ^5 - ^3
                                          push ^1
                                          call BSign
                                          ^5 = eax * 255
                                          ^3 = ^3 + ^0
                                          top eax
                                          call CMemCopy
                                          call CMemSet
                                        return

  BEquals:                              call BLengthC
                                          push eax
                                          eax = ^2 - 1
                                          eax = @eax + 0
                                          eax = eax ^ ^0
                                          if eax BEquals0Return
                                          eax = ^1 + 0
                                          ecx = ^2 + 0
    BEquals0Loop:                           edx = @eax ^ @ecx
                                            if edx BEquals0Return
                                            eax = eax + 1
                                            ecx = ecx + 1
                                            ^0 = ^0 - 1
                                          if ^0 BEquals0Loop
    BEquals0Return:                       eax = 1 + 0
                                          call BCreateR
                                          ifeq ^0 BEquals0ReturnTrue
                                            @eax = 0 + 0
                                            jump BEquals0End
    BEquals0ReturnTrue:                   @eax = 255 + 0
    BEquals0End:                          pop
                                          pop
                                          pop
                                        return

  BBEquals:                             push 0
                                          push ^2
                                          push ^2
                                          call BEquals
                                          ^0 = eax + 0
                                          eax = ^1 + 0
                                          call BDestroyR
                                          eax = ^2 + 0
                                          call BDestroyR
                                          ^2 = ^0 + 0
                                          pop
                                          pop
                                        return

  BBGreater:                            call BBSubtract
                                          push ^0
                                          call BSign
                                          if eax BBGreater0ReturnFalse
                                          call BLengthC
                                          eax = eax ^ 1
                                          ifneq eax BBGreater0ReturnTrue
                                          top eax
                                          ifeq @eax BBGreater0ReturnFalse
    BBGreater0ReturnTrue:                   call BDestroyC
                                            ^0 = 255 + 0
                                          jump BBGreater0End
    BBGreater0ReturnFalse:                  call BDestroyC
                                            ^0 = 0 + 0
    BBGreater0End:                        eax = 1 + 0
                                          call BCreateR
                                          top @eax
                                          ^0 = eax + 0
                                        return

  BBAdd:                                call BLengthC
                                          push eax
                                          eax = ^2 - 1
                                          eax = @eax + 0
                                          ecx = ^0 - eax
                                          if ecx BBAdd0Min
                                            ^0 = eax + 0
    BBAdd0Min:                            ^0 = ^0 + 1
                                          push ^0
                                          push ^0
                                          push ^3
                                          call BSignExtend
                                          push eax
                                          push ^1
                                          push ^5
                                          call BSignExtend
                                          ^1 = eax + 0
                                          eax = ^2 + 0
                                          call BCreateR
                                          push eax
                                          push 0
                                          ecx = ^2 + 0
                                          edx = ^3 + 0
    BBAdd0Loop:                             ^0 = ^0 + @edx
                                            ^0 = @ecx + ^0
                                            @eax = ^0 & 255
                                            ^0 = ^0 >> 8
                                            eax = eax + 1
                                            ecx = ecx + 1
                                            edx = edx + 1
                                            ^4 = ^4 - 1
                                          if ^4 BBAdd0Loop
                                          ^0 = ^1 + 0
                                          call BOptimize
                                          ^3 = eax + 0
                                          call BDestroy
                                          call BDestroy
                                          call BDestroy
                                          top eax
                                          ^0 = ^2 + 0
                                          ^2 = eax + 0
                                          call BDestroy
                                          call BDestroy
                                        return

  BBSubtract:                           call BLengthC
                                          push eax
                                          eax = ^2 - 1
                                          eax = @eax + 0
                                          ecx = ^0 - eax
                                          if ecx BBSubtract0Min
                                            ^0 = eax + 0
    BBSubtract0Min:                       ^0 = ^0 + 1
                                          push ^0
                                          push ^0
                                          push ^3
                                          call BSignExtend
                                          push eax
                                          push ^1
                                          push ^5
                                          call BSignExtend
                                          ^1 = eax + 0
                                          eax = ^2 + 0
                                          call BCreateR
                                          push eax
                                          push 0
                                          ecx = ^2 + 0
                                          edx = ^3 + 0
    BBSubtract0Loop:                        ^0 = ^0 + @edx
                                            ^0 = @ecx - ^0
                                            @eax = ^0 & 255
                                            ^0 = ^0 >> 8
                                            ^0 = ^0 & 1
                                            eax = eax + 1
                                            ecx = ecx + 1
                                            edx = edx + 1
                                            ^4 = ^4 - 1
                                          if ^4 BBSubtract0Loop
                                          ^0 = ^1 + 0
                                          call BOptimize
                                          ^3 = eax + 0
                                          call BDestroy
                                          call BDestroy
                                          call BDestroy
                                          top eax
                                          ^0 = ^2 + 0
                                          ^2 = eax + 0
                                          call BDestroy
                                          call BDestroy
                                        return

  BDecimalAdd16R:                       push ebx
                                          push esi
                                          push edi
                                          esi = eax + 0
                                          ebx = edx + 0
                                          eax = eax - 1
                                          eax = @eax + 0
                                          edi = eax + 0
                                          ecx = esi + 0
    BDecimalAdd16R0Loop:                    edx = @ecx + ebx
                                            ebx = edx / 10
                                            @ecx = edx % 10
                                            ecx = ecx + 1
                                            eax = eax - 1
                                          if eax BDecimalAdd16R0Loop
                                          ifeq ebx BDecimalAdd16R0End
                                            eax = edi + 1
                                            push eax
                                            push esi
                                            call BEnlarge
                                            push esi
                                            esi = eax + 0
                                            call BDestroy
                                            ecx = esi + edi
                                            @ecx = ebx + 0
    BDecimalAdd16R0End:                   eax = esi + 0
                                          top edi
                                          pop
                                          top esi
                                          pop
                                          top ebx
                                          pop
                                        return

  BDecimalMul16R:                       push ebx
                                          push esi
                                          push edi
                                          esi = eax + 0
                                          eax = eax - 1
                                          eax = @eax + 0
                                          edi = eax + 0
                                          ecx = esi + 0
                                          ebx = 0 + 0
    BDecimalMul16R0Loop:                    edx = @ecx * 16
                                            edx = edx + ebx
                                            ebx = edx / 10
                                            @ecx = edx % 10
                                            ecx = ecx + 1
                                            eax = eax - 1
                                          if eax BDecimalMul16R0Loop
                                          ifeq ebx BDecimalMul16R0End
                                            eax = edi + 1
                                            edx = ebx / 10
                                            ifeq edx BDecimalMul16R0NoMore
                                              eax = eax + 1
    BDecimalMul16R0NoMore:                   push eax
                                            push esi
                                            call BEnlarge
                                            push esi
                                            esi = eax + 0
                                            call BDestroy
                                            ecx = esi + edi
    BDecimalMul16R0OneMore:                    @ecx = ebx % 10
                                              ebx = ebx / 10
                                              ecx = ecx + 1
                                            if ebx BDecimalMul16R0OneMore
    BDecimalMul16R0End:                    eax = esi + 0
                                          top edi
                                          pop
                                          top esi
                                          pop
                                          top ebx
                                          pop
                                        return

  BBMultiply:                           call BLengthC
                                          push eax
                                          eax = ^1 - 1
                                          eax = @eax + 0
                                          ^0 = ^0 + eax
                                          push ^0
                                          push ^2
                                          call BSignExtend
                                          ecx = eax + 0
                                          eax = ^1 + 0
                                          ^1 = ecx + 0
                                          call BDestroyR
                                          push ^0
                                          push ^3
                                          call BSignExtend
                                          ecx = eax + 0
                                          eax = ^2 + 0
                                          ^2 = ecx + 0
                                          call BDestroyR
                                          eax = ^0 + 0
                                          call BCreateEmptyR
                                          push eax
                                          push 0
                                          push 0
                                          ^1 = 0 + 0
    BBMultiply0OuterLoop:                   ecx = 0 + 0
                                            ^0 = 0 + 0
    BBMultiply0InnerLoop:                     eax = ^4 + ^1
                                              edx = ^5 + ^0
                                              eax = @eax * @edx
                                              ecx = ecx + eax
                                              eax = ^0 + ^1
                                              edx = ^2 + eax
                                              ecx = ecx + @edx
                                              @edx = ecx & 255
                                              ecx = ecx >> 8
                                              ^0 = ^0 + 1
                                              eax = ^0 + ^1
                                              eax = ^3 - eax
                                            if eax BBMultiply0InnerLoop
                                            ^1 = ^1 + 1
                                            eax = ^3 - ^1
                                          if eax BBMultiply0OuterLoop
                                          eax = ^4 + 0
                                          call BDestroyR
                                          eax = ^5 + 0
                                          call BDestroyR
                                          ^0 = ^2 + 0
                                          call BOptimize
                                          ^4 = eax + 0
                                          pop
                                          call BDestroy
                                          pop
                                          pop
                                        return

  # BBDivMod(CBlob1, cBlob2: Blob): (Blob, Blob, Int); blobcall;
  BBDivMod:                             push ^0
                                          call BSign
                                          ecx = 0 + 0
                                          ifeq eax BBDivMod0NoSign0
                                            call BBNegate
                                            ecx = 3 + 0
    BBDivMod0NoSign0:                     push ecx
                                          push ^2
                                          call BSign
                                          ifeq eax BBDivMod0NoSign1
                                            ^0 = ^0 ^ 1
                                            push ^2
                                            call BBNegate
                                            ^3 = ^0 + 0
                                            pop
    BBDivMod0NoSign1:                     eax = ^2 - 1
                                          eax = @eax + 0
                                          ecx = ^2 + eax
    BBDivMod0Loop:                          eax = eax - 1
                                            ecx = ecx - 1
                                            ifneq @ecx BBDivMod0LoopDone
                                          if eax BBDivMod0Loop
                                          call EDivisionByZeroError
    BBDivMod0LoopDone:                    push eax
                                          push @ecx
                                          ifeq eax BBDivMod0NoMoreDivisor
                                            ecx = ecx - 1
                                            ^0 = ^0 << 8
                                            ^0 = ^0 + @ecx
                                            eax = eax - 1
                                            ifeq eax BBDivMod0NoMoreDivisor
                                              ^0 = ^0 + 1
    BBDivMod0NoMoreDivisor:               eax = ^3 - 1
                                          eax = @eax + 0
                                          eax = eax - 1
                                          ecx = ^3 + eax
    BBDivMod1Check:                       ifneq @ecx BBDivMod1LoopDone
                                            eax = eax - 1
                                            ecx = ecx - 1
                                          if eax BBDivMod1Check
    BBDivMod1LoopDone:                    push eax
                                          push ecx
                                          eax = eax - ^3
                                          push eax
                                          ifeq eax BBDivMod9SameLength
                                            ^0 = ^0 - 1
    BBDivMod9SameLength:                  eax = eax + 1
                                          if eax BBDivMod7Large
                                            ^0 = ^7 + 0
                                            call BDestroy
                                            eax = 1 + 0
                                            call BCreateR
                                            @eax = 0 + 0
                                            ^6 = eax + 0
                                            jump BBDivMod8Early
    BBDivMod7Large:                       call BCreateEmptyR
                                          ^0 = ^0 + eax
                                          push eax
                                          push 0
                                          push 0
                                          push 0
                                          jump BBDivMod2Check
    BBDivMod2Loop:                          ^0 = 0 + 0
                                            eax = ^5 + 0
                                            ecx = ^6 + 0
                                            edx = 3 + 0
    BBDivMod3Loop:                          ifl ecx BBDivMod3LoopDone
                                              ^0 = ^0 << 8
                                              ^0 = ^0 + @eax
                                              ecx = ecx - 1
                                              eax = eax - 1
                                              edx = edx - 1
                                            if edx BBDivMod3Loop
    BBDivMod3LoopDone:                      ^0 = ^0 / ^7
                                            ecx = ^0 >> 8
                                            ifeq ecx BBDivMod6SingleByte
                                              ^0 = ecx + 0
    BBDivMod6SingleByte:                    eax = ^4 + 0
                                            ^5 = ^5 - 1
                                            ^6 = ^6 - 1
                                            call BBDivMod0AddMul
    BBDivMod2Check:                       eax = ^6 - ^8
                                          if eax BBDivMod2Loop
                                          ifl eax BBDivMod4Done
                                            eax = @^5 + 0
                                            ifleq ^6 BBDivMod5NoAdd
                                              ecx = ^5 - 1
                                              eax = eax << 8
                                              eax = eax + @ecx
    BBDivMod5NoAdd:                         ^0 = eax / ^7
                                            ifeq ^0 BBDivMod4Done
                                              eax = ^3 + 0
                                              call BBDivMod0AddMul
    BBDivMod4Done:                        ^0 = ^11 + 0
                                          call BDestroy
                                          ^10 = ^2 + 0
                                          pop
                                          pop
                                          pop
                                          pop
    BBDivMod8Early:                       pop
                                          pop
                                          pop
                                          pop
                                        return
    BBDivMod0AddMul:              edx = ^0 + 0
    BBDivMod0AddMul0Loop:             edx = edx + @eax
                                      @eax = edx & 255
                                      eax = eax + 1
                                      edx = edx >> 8
                                    ifneq edx BBDivMod0AddMul0Loop
                                    ^2 = ^11 + 0
                                    ^1 = ^8 + 1
                                    eax = 0 + 0
                                    edx = ^5 - ^8
    BBDivMod0AddMul1Loop:             ecx = @^2 * ^0
                                      eax = eax + ecx
                                      ecx = eax & 255
                                      ecx = @edx - ecx
                                      @edx = ecx & 255
                                      ecx = ecx >> 31
                                      eax = eax >> 8
                                      eax = eax - ecx
                                      ^2 = ^2 + 1
                                      ^1 = ^1 - 1
                                      edx = edx + 1
                                    if ^1 BBDivMod0AddMul1Loop
                                    ifeq eax BBDivMod0AddMul2Check
                                      @edx = @edx - eax
                                    jump BBDivMod0AddMul2Check
    BBDivMod0AddMul2Loop:             ^6 = ^6 - 1
                                      ^5 = ^5 - 1
                                      ^4 = ^4 - 1
    BBDivMod0AddMul2Check:          ifneq @^5 BBDivMod0AddMul2SkipLoop
                                    if ^6 BBDivMod0AddMul2Loop
    BBDivMod0AddMul2SkipLoop:     return

  BBDivide:                             call BBDivMod
                                          eax = ^1 + 0
                                          call BDestroyR
                                          ^1 = ^2 + 0
                                          ^0 = ^0 & 1
                                          ifeq ^0 BBDivide0Optimize
                                            ^0 = ^2 + 0
                                            call BNegate
                                          jump BBDivide0End
    BBDivide0Optimize:                      ^0 = ^2 + 0
                                            call BOptimize
    BBDivide0End:                         ^1 = eax + 0
                                          call BDestroy
                                        return

  BBModulo:                             call BBDivMod
                                          eax = ^2 + 0
                                          call BDestroyR
                                          ^0 = ^0 & 2
                                          ifeq ^0 BBModulo0Optimize
                                            ^0 = ^1 + 0
                                            call BNegate
                                          jump BBModulo0End
    BBModulo0Optimize:                      ^0 = ^1 + 0
                                            call BOptimize
    BBModulo0End:                         ^1 = eax + 0
                                          call BDestroy
                                        return

  BIntToStr:                            push ebx
                                          push esi
                                          push edi
                                          edi = eax + 0
                                          eax = eax - 1
                                          eax = @eax + 0
                                          edx = edi + eax
                                          edx = edx - 1
                                          ebx = @edx >> 7
                                          if ebx BIntToStr0Negate
                                            esi = edi + 0
                                          jump BIntToStr0SignDone
    BIntToStr0Negate:                       push edi
                                            call BNegate
                                            esi = eax + 0
    BIntToStr0SignDone:                   eax = esi - 1
                                          eax = @eax + 0
                                          edi = eax + 0
                                          esi = esi + eax
                                          eax = 1 + 0
                                          call BCreateR
                                          @eax = 0 + 0
    BIntToStr0Loop:                         esi = esi - 1
                                            call BDecimalMul16R
                                            edx = @esi >> 4
                                            call BDecimalAdd16R
                                            call BDecimalMul16R
                                            edx = @esi & 15
                                            call BDecimalAdd16R
                                            edi = edi - 1
                                          if edi BIntToStr0Loop
                                          edi = eax + 0
                                          eax = eax - 1
                                          eax = @eax + 0
                                          eax = eax + ebx
                                          call BCreateR
                                          push eax
                                          ifeq ebx BIntToStr0NoSign
                                            eax = esi + 0
                                            call BDestroyR
                                            top eax
                                            @eax = 45 + 0
    BIntToStr0NoSign:                     eax = edi - 1
                                          eax = @eax + 0
                                          esi = eax + 0
                                          edi = edi + eax
                                          top ecx
                                          ebx = ecx + ebx
    BIntToStr1Loop:                         edi = edi - 1
                                            @ebx = @edi + 48
                                            ebx = ebx + 1
                                            esi = esi - 1
                                          if esi BIntToStr1Loop
                                          eax = edi + 0
                                          call BDestroyR
                                          top eax
                                          pop
                                          top edi
                                          pop
                                          top esi
                                          pop
                                          top ebx
                                          pop
                                        return

  BBPrintStr:                           call BLengthC
                                          top ecx
    BBPrintStr0Loop:                        writechar @ecx
                                            ecx = ecx + 1
                                            eax = eax - 1
                                          if eax BBPrintStr0Loop
                                          call BDestroy
                                        return

  BBPrintInt:                            top eax
                                          call BIntToStr
                                          push eax
                                          call BBPrintStr
                                          call BDestroy
                                        return

  BBgetchr:                              eax = 1 + 0
                                          call BCreateR
                                          readchar @eax
                                          push eax
                                        return

  BBgetint:                             readint eax
                                          push eax
                                          call BBToBlob
                                        return

  BBoutstr:                             call BLengthC
                                          top ecx
    BBoutstr0Loop:                          eax = eax - 1
                                            edx = @ecx - 37
                                            ifneq edx BBoutstr0WriteChar
                                              ifeq eax EoutstrError
                                              eax = eax - 1
                                              ecx = ecx + 1
                                              edx = @ecx - 37
                                              ifeq edx BBoutstr0WriteChar
                                              edx = @ecx - 115
                                              ifeq edx BBoutstr0Percents
                                              edx = @ecx - 100
                                              ifeq edx BBoutstr0Percentd
                                              jump EoutstrError
    BBoutstr0Percents:                          call BBoutstr0GetPar
                                                call BBPrintStr
                                              jump BBoutstr0ClearStack
    BBoutstr0Percentd:                          call BBoutstr0GetPar
                                                call BBPrintInt
    BBoutstr0ClearStack:                      top ecx
                                              pop
                                              top eax
                                              pop
                                              ^1 = ^0 + 0
                                              pop
                                              jump BBoutstr0Continue
    BBoutstr0WriteChar:                     writechar @ecx
    BBoutstr0Continue:                      ecx = ecx + 1
                                          if eax BBoutstr0Loop
                                          call BDestroy
                                        return
    BBoutstr0GetPar:                      push eax
                                            push ecx
                                            push ^3
                                          return


  BFinalizeResult:                      eax = ebx + 0
                                          ecx = eax + 1
                                          ifneq ecx BFinalizeResult0End
                                            eax = 1 + 0
                                            call BCreateR
                                            @eax = 0 + 0
    BFinalizeResult0End:                  push eax
                                        return

  BBIsZero:                             call BLengthC
                                          ecx = eax - 1
                                          ifneq ecx BBIsZero0Return1
                                          ifneq @^0 BBIsZero0Return1
                                            eax = 0 + 0
                                          jump BBIsZero0End
    BBIsZero0Return1:                       eax = 1 + 0
    BBIsZero0End:                         push ^0
                                          ^1 = eax + 0
                                          call BDestroy
                                          top eax
                                          pop
                                        return

  BBTrue:                               push 1
                                          call BBCreate
                                          @eax = 255 + 0
                                        return

  BBFalse:                              push 1
                                          call BBCreate
                                          @eax = 0 + 0
                                        return

