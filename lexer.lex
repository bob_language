%{

#include <string.h>
#include <stdio.h>
#include "dynstrings.h"
#include "core.h"
#include "parser.h"

int yycolumn = 1;

void set_yylloc() {
	yylloc.first_line = yylloc.last_line = yylineno;
	yylloc.first_column = yycolumn;
	yylloc.last_column = yycolumn + yyleng - 1;
	yycolumn += yyleng;
}

#define YY_USER_ACTION set_yylloc();

%}

%option yylineno

DECNUM     [0-9]+
HEXNUM     "0x"+[0-9A-Fa-f]+
STRNUM     \"[^\"\n]+\"
ID         [a-zA-Z][0-9A-Za-z_]*
PARS       "("|")"|"{"|"}"|"["|"]"
OPERATOR   "+"|"-"|"*"|"/"|"%"|"&"|"|"|"^"|"!"|">>"|"<<"|"<"|">"|"=="|"$"|"="|","
COMMENT    "#".*$

%%

fn {
  return FN;
}
\? {
  yylval.node = node_new();
  yylval.node->id = NODE_IF;
  return IF;
}
\?\? {
  yylval.node = node_new();
  yylval.node->id = NODE_WHILE;
  return WHILE;
}
re {
  yylval.node = node_new();
  yylval.node->id = NODE_RETURN;
  return RETURN;
}
TRUE {
  yylval.node = node_new();
  yylval.node->id = NODE_PUSH_BOOL;
  node_set(yylval.node, strdup("True"));
  return TRUE;
}
FALSE {
  yylval.node = node_new();
  yylval.node->id = NODE_PUSH_BOOL;
  node_set(yylval.node, strdup("False"));
  return FALSE;
}
{DECNUM} {
  yylval.node = node_new();
  yylval.node->id = NODE_PUSH_DEC;
  node_set(yylval.node, strdup(yytext));
  return DECNUM;
}
{HEXNUM} {
  yylval.node = node_new();
  yylval.node->id = NODE_PUSH_HEX;
  node_set(yylval.node, strdup(yytext));
  return HEXNUM;
}
{STRNUM} {
  yylval.node = node_new();
  yylval.node->id = NODE_PUSH_STR;
  node_set(yylval.node, strdup(yytext));
  return STRNUM;
}
{ID} {
  yylval.node = node_new();
  yylval.node->id = NODE_PUSH_VAR;
  node_set(yylval.node, strdup(yytext));
  return ID;
}
{PARS} {
  return yytext[0];
}
{OPERATOR} {
  if (strcmp(yytext, "<<") == 0) return SHL;
  if (strcmp(yytext, ">>") == 0) return SHR;
  if (strcmp(yytext, "==") == 0) return EQU;
  return yytext[0];
}
[ \t] ;
{COMMENT} ;
\n {
  yycolumn = 1;
  return NEWLINE;
}
. {
  core_error(str_printf("Error: [Line %d] Illegal character! (\"%s\")\n", yylineno, yytext));
}

%%

