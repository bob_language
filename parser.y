%locations
%error-verbose
%{

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "core.h"
#include "parser.h"
#include "dynstrings.h"

int yylex (void);
int yyparse (void);
void yyerror (char *err,...);
void yyerror_loc (YYLTYPE *yylloc,char *err,...);

extern FILE *yyin;
extern FILE *c_output;
extern TreeNode *root;

%}

%union{
  TreeNode *node;
}

%token FN NEWLINE SHL SHR EQU
%token<node> IF WHILE RETURN TRUE FALSE DECNUM HEXNUM STRNUM ID

%type<node> blob get_length get_element expr assign call_args callfn line startline codeblock function func_args init

%left ','
%left SHL SHR
%left '<' '>' EQU
%left '+' '-' '&' '|'
%left '*' '/' '%' '^'
%left NEG
%left PARS

%start init

%%

init: /* EPSILON */ { $$ = NULL; }
    | NEWLINE init { $$ = NULL; }
    | function init { $$ = node_new(); $$->id = NODE_ROOT; node_bottom($$, $1); node_next(root, $$); }
;

function: FN ID '(' func_args ')' codeblock { $$ = node_new(); $$->id = NODE_FUNCTION; node_next($$, $4); node_next($$, $2); node_bottom($$, $6); }
        | FN ID '(' ')' codeblock { $$ = node_new(); $$->id = NODE_FUNCTION; node_next($$, $2); node_bottom($$, $5); }
;

func_args: ID { $$ = node_new(); $$->id = NODE_FUNC_ARGS; node_bottom($$, $1); }
         | ID ',' func_args { $$ = node_new(); $$->id = NODE_FUNC_ARGS; node_next($$, $3); node_bottom($$, $1); }
;

codeblock: '{' NEWLINE startline '}' NEWLINE { $$ = node_new(); $$->id = NODE_CODEBLOCK; node_bottom($$, $3); }
         | NEWLINE '{' NEWLINE startline '}' NEWLINE { $$ = node_new(); $$->id = NODE_CODEBLOCK; node_bottom($$, $4); }
;

startline: /* EPSILON */ { $$ = node_new(); $$->id = NODE_LINE; node_bottom($$, node_new()); }
         | NEWLINE startline { $$ = $2; }
         | line startline { $$ = node_new(); $$->id = NODE_LINE; node_next($$, $2); node_bottom($$, $1); }
;

line: RETURN expr NEWLINE { $$ = $1; node_bottom($$, $2); }
    | assign NEWLINE { $$ = $1; }
    | callfn NEWLINE { $$ = node_new(); $$->id = NODE_VOID_RETURN; node_bottom($$, $1); }
    | IF '(' expr ')' codeblock { $$ = $1; node_next($$, $3); node_bottom($$, $5); }
    | WHILE '(' expr ')' codeblock { $$ = $1; node_next($$, $3); node_bottom($$, $5); }
;

blob: ID
    | TRUE
    | FALSE
    | DECNUM
    | HEXNUM
    | STRNUM
    | callfn
;

callfn: ID '(' call_args ')' { $$ = node_new(); $$->id = NODE_CALLFN; node_bottom($$, $3); node_bottom($$, $1); }
      | ID '(' ')' { $$ = node_new(); $$->id = NODE_CALLFN; node_bottom($$, $1); }
;

call_args: expr { $$ = node_new(); $$->id = NODE_CALL_ARGS; node_bottom($$, $1); }
         | expr ',' call_args { $$ = node_new(); $$->id = NODE_CALL_ARGS; node_bottom($$, $1); node_next($$, $3); }
;

expr: blob
    | expr '+' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Add")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '-' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Subtract")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '*' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Multiply")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '/' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Divide")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '%' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Modulo")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '^' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Xor")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '&' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("And")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '|' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Or")); node_bottom($$, $1); node_bottom($$, $3); }
    | '!' expr %prec NEG { $$ = node_new(); $$->id = NODE_ARIT_UNARY; node_set($$, strdup("Not")); node_bottom($$, $2); }
    | '-' expr %prec NEG { $$ = node_new(); $$->id = NODE_ARIT_UNARY; node_set($$, strdup("Negate")); node_bottom($$, $2); }
    | expr SHL expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("ShiftLeft")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr SHR expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("ShiftRight")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '>' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Greater")); node_bottom($$, $1); node_bottom($$, $3); }
    | expr '<' expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Greater")); node_bottom($$, $3); node_bottom($$, $1); }
    | expr EQU expr { $$ = node_new(); $$->id = NODE_ARIT_BINARY; node_set($$, strdup("Equals")); node_bottom($$, $1); node_bottom($$, $3); }
    | get_element
    | '$' get_length { $$ = node_new(); $$->id = NODE_GET_LENGTH; node_bottom($$, $2); }
    | '(' expr ')' %prec PARS { $$ = $2; }
;

get_length: '$' get_length { $$ = node_new(); $$->id = NODE_GET_LENGTH; node_bottom($$, $2); }
          | '(' expr ')' { $$ = $2; }
          | blob
;

get_element: get_length '[' expr ']' { $$ = node_new(); $$->id = NODE_GET_ELEMENT; node_bottom($$, $1); node_bottom($$, $3); }
           | get_element '[' expr ']' { $$ = node_new(); $$->id = NODE_GET_ELEMENT; node_bottom($$, $1); node_bottom($$, $3); }
;

assign: ID '=' expr { $$ = node_new(); $$->id = NODE_SET_VARIABLE; node_bottom($$, $1); node_bottom($$, $3); }
      | ID '[' expr ']' '=' expr { $$ = node_new(); $$->id = NODE_SET_ELEMENT; node_bottom($$, $1); node_bottom($$, $3); node_bottom($$, $6); }
      | '$' ID '=' expr { $$ = node_new(); $$->id = NODE_SET_LENGTH; node_bottom($$, $2); node_bottom($$, $4); }
;

%%

void yyerror(char *err, ...) {
  va_list list;
  va_start(list, err);
  core_error(str_printf("Error (at position %d:%d-%d:%d) : ", yylloc.first_line, yylloc.first_column, yylloc.last_line, yylloc.last_column));
  core_error(str_vprintf(err, list));
  core_error(strdup("\n"));
  va_end(list);
}

void yyerrorloc(YYLTYPE *yylloc,char *err, ...) {
  va_list list;
  va_start(list, err);
  core_error(str_printf("Error (at position %d:%d-%d:%d) : ", yylloc->first_line, yylloc->first_column, yylloc->last_line, yylloc->last_column));
  core_error(str_vprintf(err, list));
  core_error(strdup("\n"));
  va_end(list);
}

int main(int argc, char **argv) {
  core_initialize();
  FILE *input;
  if (argc > 1) {
    input = fopen(argv[1], "r");
    if (input == NULL) {
      fprintf(stderr, "Error opening file \"%s\"\n", argv[1]);
      return 1;
    }
  }
  else {
    input = stdin;
  }
  yyin = input;
  c_output = stdout;
  yylloc.first_line = yylloc.last_line = 1;
  yylloc.first_column = yylloc.last_column = 1;
  yyparse();
  root_output();
  core_finalize();
  fclose(input);
  return 0;
}

