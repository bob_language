#ifndef CORE_H
#define CORE_H

#define NODE_EMPTY         0
#define NODE_ROOT          1
#define NODE_FUNCTION      2
#define NODE_FUNC_ARGS     3
#define NODE_RETURN        4
#define NODE_IF            5
#define NODE_WHILE         6
#define NODE_PUSH_VAR      7
#define NODE_PUSH_BOOL     8
#define NODE_PUSH_DEC      9
#define NODE_PUSH_HEX     10
#define NODE_PUSH_STR     11
#define NODE_CALLFN       12
#define NODE_CALL_ARGS    13
#define NODE_VOID_RETURN  14
#define NODE_ARIT_BINARY  15
#define NODE_ARIT_UNARY   16
#define NODE_SET_VARIABLE 17
#define NODE_GET_LENGTH   18
#define NODE_SET_LENGTH   19
#define NODE_GET_ELEMENT  20
#define NODE_SET_ELEMENT  21
#define NODE_LINE         22
#define NODE_CODEBLOCK    23

#define NODE_MAXFUNC      24

struct _TreeNode {
  int id;
  char *code;
  struct _TreeNode *next;
  struct _TreeNode *previous;
  struct _TreeNode *bottom;
};
typedef struct _TreeNode TreeNode;

void core_initialize();
void core_finalize();
void core_error(char *msg);

TreeNode *node_new();
void node_set(TreeNode *node, char *code);
void node_next(TreeNode *parent, TreeNode *child);
void node_bottom(TreeNode *parent, TreeNode *child);
void root_output();

#endif

