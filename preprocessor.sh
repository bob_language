#!/bin/sh

echo "Preprocessing code.asm..."

outfile="assembly.c"
outfileh="assembly.h"

echo "#include <stdio.h>" > $outfile
echo "" >> assembly.c
echo "void asm_print(FILE *output) {" >> $outfile
sed 's/\(^.*$\)/  fputs(\"\1\\n", output);/' code.asm >> $outfile
echo "}" >> $outfile

echo "#ifndef ASSEMBLY_H" > $outfileh
echo "#define TABLES_H" >> $outfileh
echo "" >> $outfileh
echo "void asm_print(FILE *output);" >> $outfileh
echo "" >> $outfileh
echo "#endif" >> $outfileh

