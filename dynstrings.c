#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

char *str_printf(char *format, ...) {
  va_list arguments, argcopy;
  va_start(arguments, format);
  va_copy(argcopy, arguments);
  int size = vsnprintf(NULL, 0, format, arguments);
  char *result = malloc(size + 1);
  vsprintf(result, format, argcopy);
  va_end(arguments);
  return result;
}

char *str_vprintf(char *format, va_list arguments) {
  va_list argcopy;
  va_copy(argcopy, arguments);
  int size = vsnprintf(NULL, 0, format, arguments);
  char *result = malloc(size + 1);
  vsprintf(result, format, argcopy);
  return result;
}

char *str_cat(char *arg1, char *arg2) {
  char *result = str_printf("%s%s", arg1, arg2);
  free(arg1);
  return result;
}

char *str_cat2(char *arg1, char *arg2) {
  char *result = str_printf("%s%s", arg1, arg2);
  free(arg1);
  free(arg2);
  return result;
}

