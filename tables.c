
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "tables.h"

int maxVars = 0;
Variable *globals;
Variable *locals;
int maxFunctions = 0;
Function *functions;
int maxCalls = 0;
Call *calls;

void reallocFunctions(int newSize){
  functions = realloc(functions, newSize * sizeof(Function));

  int i;
  for (i = maxFunctions; i < newSize; i++) {
    functions[i].name = NULL;
    functions[i].paramcount= 0;
  }

  maxFunctions = newSize;
}

void reallocVariables(int newSize){
  globals = realloc(globals, newSize * sizeof(Variable));
  locals = realloc(locals, newSize * sizeof(Variable));

  int i;
  for (i = maxVars; i < newSize; i++) {
    globals[i].name = NULL;
    globals[i].tname = NULL;
    globals[i].pos = 0;
    locals[i].name = NULL;
    locals[i].tname = NULL;
    locals[i].pos= 0;
  }

  maxVars = newSize;
}

void clearLocals(){
  int i;
  for (i = 0; i < maxVars; i++){
    free(locals[i].name);
    free(locals[i].tname);
    locals[i].name = NULL;
    locals[i].tname = NULL;
    locals[i].pos = 0;
  }
}

void clearGlobals(){
  int i;
  for (i = 0; i < maxVars; i++){
    free(globals[i].name);
    free(globals[i].tname);
    globals[i].name = NULL;
    globals[i].tname = NULL;
    globals[i].pos = 0;
  }
}

void clearFunctions(){
  //destruktor - funkcie sa uz nebudu dat pridavat
  int i = 0;
  while ((i < maxFunctions) && (functions[i].name != NULL)) {
    free(functions[i].name);
    i++;
  }
  functions = realloc(functions, 0);
  maxFunctions = 0;
}

void destroyVariables(){
  reallocVariables(0);
}

int varLocal(char *name){
  int i;
  for (i = 0; i < maxVars; i++){
    if (locals[i].name != NULL && strcmp(locals[i].name, name) == 0){
      return locals[i].pos;
    }
  }
  return -1;
}

char *varGlobal(char *name){
  int i;
  for (i = 0; i < maxVars; i++){
    if (globals[i].name != NULL && strcmp(globals[i].name, name) == 0){
      return globals[i].tname;
    }
  }
  return NULL;
}

char *varGlobalNo(int i){
  if (maxVars <= i){
    return NULL;
  }

  return globals[i].tname;
}

int varFunction(char *name){
  int i;
  for (i = 0; i < maxFunctions; i++){
    if (functions[i].name != NULL && strcmp(functions[i].name, name) == 0){
      return functions[i].paramcount;
    }
  }
  return -1;
}

int varFunctionCall(char *name){
  int pos = 0;
  while ((pos < maxCalls) && (calls[pos].name != NULL)){
    if (strcmp(name, calls[pos].name) == 0){
      return calls[pos].paramcountSize;
    }
    pos++;
  }

  return 0;
}

void varInvalidFunctionCalls(char ***names, int *n){
  char **invfns = malloc(maxCalls);
  int i = 0;
  int pos = 0;
  while ( (i < maxCalls) && (calls[i].name != NULL) ){
    if ((calls[i].paramcountSize > 1)
        || (calls[i].paramcount[0] != varFunction(calls[i].name))
        || (varFunction(calls[i].name) == -1)){
      invfns[pos++] = calls[i].name;
    }
    i++;
  }

  *names = invfns;
  *n = pos;
}

void addFunctionCall(char *name, int paramcount){
  int pos = 0;
  while ((pos < maxCalls) 
      && (calls[pos].name != NULL)
      && (strcmp(calls[pos].name, name) != 0)) {
    pos++;
  }

  if (pos == maxCalls) {
    //uz ma nebavilo pisat novu funkciu na zvacsenie a vynulovanie
    int newSize = (maxCalls + 1) * 2;
    calls = realloc(calls, newSize * sizeof(Call));
    int i;
    for (i = pos; i < newSize; i++){
      calls[i].name = NULL;
      calls[i].paramcountSize = 0;
    }

    maxCalls = newSize;
  }

  calls[pos].name = strdup(name);

  //fuj
  int paramPos = 0;
  while (
      (paramPos < calls[pos].paramcountSize)
      && (calls[pos].paramcount[paramPos] != paramcount))
  {
    paramPos++;
  }

  if (paramPos == calls[pos].paramcountSize){
    calls[pos].paramcountSize++;
  }

  calls[pos].paramcount[paramPos] = paramcount;
}

void pushVariable(char *name, int isGlobal){
  Variable *vars;
  char *prefix;
  if (isGlobal){
    vars = globals;
    prefix = "glb";
  } else {
    vars = locals;
    prefix = "loc";
  }


  int i = 0;
  while ((i < maxVars) && (vars[i].name != NULL)) {
    i++;
  }

  if (i == maxVars) {
    reallocVariables(maxVars * 2 + 1);
    //zmenili sa nam smerniky
    if (isGlobal){
        vars = globals;
      } else {
        vars = locals;
      }
  }

  vars[i].pos = i;

  vars[i].name = strdup(name);

  vars[i].tname = malloc(sizeof(char) * (strlen(name) + strlen(prefix) + 1));
  strcpy(vars[i].tname, prefix);
  strcat(vars[i].tname, name);

}

void addLocal(char *name){
  pushVariable(name, 0);
}

void addGlobal(char *name){
  pushVariable(name, 1);
}

void addFunction(char *name, int paramcount){

  int i = 0;
  while ((i < maxFunctions) && (functions[i].name != NULL)) {
    i++;
  }

  if (i == maxFunctions) {
    reallocFunctions(maxFunctions * 2 + 1);
  }

  functions[i].name = strdup(name);

  functions[i].paramcount = paramcount;

}

/*
int main(){
  int i;

  addFunctionCall("macka", 3);
  addFunctionCall("kacka", 3);
  addFunctionCall("kacka", 1);
  addFunctionCall("huska", 3);
  addFunctionCall("macka", 1);
  addFunctionCall("macka", 3);
  addFunctionCall("kacka", 2);
  addFunctionCall("macka", 3);
  addFunctionCall("kacka", 4);
  addFunctionCall("1", 3);
  addFunctionCall("1", 3);
  addFunctionCall("2", 3);
  addFunctionCall("2", 4);
  addFunctionCall("3", 3);
  addFunctionCall("4", 3);
  addFunctionCall("5", 3);
  addFunctionCall("6", 3);
  printf("pocty argumentov pre kacka: %d\n", varFunctionCall("kacka"));
  printf("pocty argumentov pre macka: %d\n", varFunctionCall("macka"));
  printf("pocty argumentov pre huska: %d\n", varFunctionCall("huska"));
  printf("pocty argumentov pre neni: %d\n", varFunctionCall("neni"));

  int n;
  char **nazvy;
  varInvalidFunctionCalls(&nazvy, &n);
  for (i = 0; i < n; i++){
    printf("nespravne volana fcia %s\n", nazvy[i]);
  }

  return 0;

  addLocal("a");
  addLocal("b");
  addLocal("c");
  printf("loc a %d\n", varLocal("a"));
  printf("loc b %d\n", varLocal("b"));
  printf("loc c %d\n", varLocal("c"));
  printf("loc d %d\n", varLocal("d"));
  clearLocals();
  addLocal("c");
  addLocal("b");
  addLocal("a");
  printf("loc c %d\n", varLocal("c"));
  printf("loc b %d\n", varLocal("b"));
  printf("loc a %d\n", varLocal("a"));

  addGlobal("A");
  addGlobal("B");
  addGlobal("C");
  printf("glob A %s\n", varGlobal("A"));
  printf("glob B %s\n", varGlobal("B"));
  printf("glob C %s\n", varGlobal("C"));
  printf("glob D %s\n", varGlobal("D"));

  char *name;
  i = 0;
  while ((name = varGlobalNo(i++)) != NULL ){
    printf("globalna iterovana %s\n", name);
  }

  return 0;

  addFunction("kacka", 2);
  addFunction("macka", 2);
  addFunction("frantisek", 3);
  addFunction("fero", 0);

  printf("fero: %d\n", varFunction("fero"));
  printf("frantisek: %d\n", varFunction("frantisek"));
  printf("kacka: %d\n", varFunction("kacka"));
  addFunction("macka1", 2);
  addFunction("macka11", 2);
  printf("fero: %d\n", varFunction("fero"));
  printf("frantisek: %d\n", varFunction("frantisek"));
  printf("kacka: %d\n", varFunction("kacka"));
  printf("neni: %d\n", varFunction("neni"));
}
*/
