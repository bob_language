#ifndef DYNSTRINGS_H
#define DYNSTRINGS_H

#include <stdarg.h>

char *str_printf(char *format, ...);
char *str_vprintf(char *format, va_list arguments);
char *str_cat(char *arg1, char *arg2);
char *str_cat2(char *arg1, char *arg2);

#endif

