#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dynstrings.h"
#include "tables.h"
#include "core.h"
#include "numbers.h"
#include "assembly.h"

#define SUPRESS_WARNING(x) (void)x

int c_label = 0;
int c_stack = 0;

char *c_fnname;
char *c_error;
char c_pad[256];
FILE *c_output;
void (*nodefunc[NODE_MAXFUNC]) (TreeNode *node);
TreeNode *root;

TreeNode *node_new() {
  TreeNode *result = malloc(sizeof(TreeNode));
  result->id = NODE_EMPTY;
  result->code = strdup("");
  result->next = result;
  result->previous = result;
  result->bottom = NULL;
  return result;
}

void node_free(TreeNode *node) {
  free(node->code);
  free(node);
}

void node_set(TreeNode *node, char *code) {
  free(node->code);
  node->code = code;
}

void node_next(TreeNode *parent, TreeNode *child) {
  child->previous->next = parent->next;
  parent->next->previous = child->previous;
  parent->next = child;
  child->previous = parent;
}

void node_bottom(TreeNode *parent, TreeNode *child) {
  if (parent->bottom == NULL) {
    parent->bottom = child;
  }
  else {
    TreeNode *childprev = child->previous;
    TreeNode *parentprev = parent->bottom->previous;
    child->previous = parentprev;
    parent->bottom->previous = childprev;
    childprev->next = parent->bottom;
    parentprev->next = child;
    parent->bottom = child;
  }
}

void node_traverse(TreeNode *node) {
  nodefunc[node->id](node);
}

void core_error(char *msg) {
  c_error = str_cat2(c_error, msg);
}

void core_pad_add(int amount) {
  int start = strlen(c_pad);
  int i;
  for (i = 0; i < amount; i++)
    c_pad[start + i] = ' ';
  c_pad[start + amount] = '\0';
}

void core_pad_sub(int amount) {
  int position = strlen(c_pad) - amount;
  if (position < 0) position = 0;
  c_pad[position] = '\0';
}

void core_pad_reset() {
  c_pad[0] = '\0';
}

char *core_translate_name(char *name) {
  int length = strlen(name);
  int i;
  int translate = 0;
  for (i = 0; i < length; i++) {
    if ((name[i] == '8') || (name[i] == '9') || (name[i] == '_'))
      translate++;
  }
  char *result = malloc(length + translate + 1);
  char *output = result;
  for (i = 0; i < length; i++) {
    switch (name[i]) {
      case '8':
        output[1] = output[0] = '8';
        output += 2;
        break;
      case '9':
        output[1] = output[0] = '9';
        output += 2;
        break;
      case '_':
        output[0] = '8';
        output[1] = '9';
        output += 2;
        break;
      default:
        output[0] = name[i];
        output++;
    }
  }
  output[0] = '\0';
  return result;
}

char *core_translate_var(char *name) {
  name = core_translate_name(name);
  char *result;
  int local = varLocal(name);
  if (local < 0) {
    result = varGlobal(name);
    if (result == NULL) {
      addGlobal(name);
      result = varGlobal(name);
    }
    return strdup(result);
  }
  else
    return str_printf("^%d", local + c_stack);
}

void core_empty(TreeNode *node) {
  SUPRESS_WARNING(node);
};

void core_function(TreeNode *node) {
  clearLocals();
  c_stack = 0;
  int c_params = 0;
  TreeNode *iterator = node->next->next;
  while (iterator != node) {
    node_traverse(iterator);
    iterator = iterator->next;
    node_free(iterator->previous);
    c_params++;
  }
  c_fnname = node->next->code;
  if (varFunction(c_fnname) != -1) {
    core_error(str_printf("Error: Function %s already exists!\n", c_fnname));
    return;
  }
  addFunction(c_fnname, c_params);
  c_fnname = core_translate_name(c_fnname);
  node_free(node->next);
  core_pad_reset();
  fprintf(c_output, "fn%sStart:\n", c_fnname);
  core_pad_add(2);
  fprintf(c_output, "%sebx = 0 - 1\n", c_pad);
  node_traverse(node->bottom);
  fputs(node->bottom->code, c_output);
  node_free(node->bottom);
  fprintf(c_output, "jump fn%sEnd\n", c_fnname);
  fprintf(c_output, "fn%sEnd:\n", c_fnname);
  int i;
  for (i = 0; i < c_params; i++)
    fprintf(c_output, "  call BDestroy\n");
  fprintf(c_output, "  call BFinalizeResult\n");
  fprintf(c_output, "return\n\n");
  free(c_fnname);
  if (c_stack != 0)
    core_error(str_printf("Error: Stack inconsistent! (%d)\n", c_stack));
}

void core_func_args(TreeNode *node) {
  char *name = core_translate_name(node->bottom->code);
  addLocal(name);
  free(name);
  node_free(node->bottom);
}

void core_return(TreeNode *node) {
  node_traverse(node->bottom);
  char *result = strdup(node->bottom->code);
  result = str_cat2(result, str_printf("%stop ebx\n", c_pad));
  result = str_cat2(result, str_printf("%spop\n", c_pad));
  result = str_cat2(result, str_printf("jump fn%sEnd\n", c_fnname));
  node_set(node, result);
  node_free(node->bottom);
  c_stack--;
}

void core_if(TreeNode *node) {
  node_traverse(node->next);
  c_stack--;
  core_pad_add(2);
  node_traverse(node->bottom);
  core_pad_sub(2);
  char *label = str_printf("fn%s%d", c_fnname, c_label);
  char *result = strdup(node->next->code);
  result = str_cat2(result, str_printf("%scall BBIsZero\n", c_pad));
  result = str_cat2(result, str_printf("%sifeq eax %s\n", c_pad, label));
  result = str_cat(result, node->bottom->code);
  result = str_cat2(result, str_printf("%s%s: eax = eax + 0\n", c_pad, label));
  node_set(node, result);
  free(label);
  node_free(node->next);
  node_free(node->bottom);
  c_label++;
}

void core_while(TreeNode *node) {
  core_pad_add(2);
  node_traverse(node->next);
  c_stack--;
  node_traverse(node->bottom);
  core_pad_sub(2);
  char *check = str_printf("%sfn%dCheck", c_fnname, c_label);
  char *loop = str_printf("%sfn%dLoop", c_fnname, c_label);
  char *result = str_printf("%sjump %s\n", c_pad, check);
  result = str_cat2(result, str_printf("%s%s:\n", c_pad, loop));
  result = str_cat(result, node->bottom->code);
  result = str_cat2(result, str_printf("%s%s:\n", c_pad, check));
  result = str_cat(result, node->next->code);
  result = str_cat2(result, str_printf("%s  call BBIsZero\n", c_pad));
  result = str_cat2(result, str_printf("%sifneq eax %s\n", c_pad, loop));
  node_set(node, result);
  free(check);
  free(loop);
  node_free(node->next);
  node_free(node->bottom);
  c_label++;
}

void core_push_var(TreeNode *node) {
  char *name = core_translate_var(node->code);
  char *result = str_printf("%spush %s\n", c_pad, name);
  free(name);
  result = str_cat2(result, str_printf("%scall BBCopy\n", c_pad));
  node_set(node, result);
  c_stack++;
}

void core_push_bool(TreeNode *node) {
  node_set(node, str_printf("%scall BB%s\n", c_pad, node->code));
  c_stack++;
}

void core_push_dec(TreeNode *node) {
  unsigned char* str;
  int length;
  decToBlob(node->code, &str, &length);
  char *result = str_printf("%spush %d\n", c_pad, length);
  result = str_cat2(result, str_printf("%scall BBCreate\n", c_pad));
  int i;
  for (i = 0; i < length; i++) {
    result = str_cat2(result, str_printf("%s@eax = %d + 0\n", c_pad, str[i]));
    if ((i + 1) < length)
      result = str_cat2(result, str_printf("%seax = eax + 1\n", c_pad));
  }
  node_set(node, result);
  free(str);
  c_stack++;
}

void core_push_hex(TreeNode *node) {
  unsigned char* str;
  int length;
  hexToBlob(node->code, &str, &length);
  char *result = str_printf("%spush %d\n", c_pad, length);
  result = str_cat2(result, str_printf("%scall BBCreate\n", c_pad));
  int i;
  for (i = 0; i < length; i++) {
    result = str_cat2(result, str_printf("%s@eax = %d + 0\n", c_pad, str[i]));
    if ((i + 1) < length)
      result = str_cat2(result, str_printf("%seax = eax + 1\n", c_pad));
  }
  node_set(node, result);
  free(str);
  c_stack++;
}

void core_push_str(TreeNode *node) {
  char *str = node->code;
  char *input = str + 1;
  char *output = str;
  int escape = 0;
  while (input[0] != '"') {
    if (escape) {
      switch (input[0]) {
        case '\\':
          output[0] = '\\';
          break;
        case 'q':
          output[0] = '"';
          break;
        case 'n':
          output[0] = '\n';
          break;
        case 't':
          output[0] = '\t';
          break;
        default:
          core_error(str_printf("Error: Invalid escape character! (\"%c\")\n", input[0]));
      }
      escape = 0;
      output++;
    }
    else {
      if (input[0] != '\\') {
        output[0] = input[0];
        output++;
      }
      else
        escape = 1;
    }
    input++;
  }
  if (escape != 0)
    core_error(strdup("Error: Dangling escape character!\n"));
  output[0] = '\0';
  int length = (output - str);
  char *result = str_printf("%spush %d\n", c_pad, length);
  result = str_cat2(result, str_printf("%scall BBCreate\n", c_pad));
  int i;
  for (i = 0; i < length; i++) {
    result = str_cat2(result, str_printf("%s@eax = %d + 0\n", c_pad, str[i]));
    if ((i + 1) < length)
      result = str_cat2(result, str_printf("%seax = eax + 1\n", c_pad));
  }
  node_set(node, result);
  c_stack++;
}

void core_callfn(TreeNode *node) {
  char *params = strdup("");
  int c_callargs = 0;
  TreeNode *iterator = node->bottom->previous;
  while (iterator != node->bottom) {
    node_traverse(iterator);
    params = str_cat(params, iterator->code);
    c_callargs++;
    iterator = iterator->previous;
    node_free(iterator->next);
  }
  char *name = node->bottom->code;
  char *result = params;
  if (strcmp(name, "get_chr") == 0) {
    if (c_callargs != 0)
      core_error(str_printf("Error: get_chr should have 0 parameters! (has %d)\n", c_callargs));
    result = str_cat2(result, str_printf("%scall BBgetchr\n", c_pad));
  }
  else if (strcmp(name, "get_int") == 0) {
    if (c_callargs != 0)
      core_error(str_printf("Error: get_int should have 0 parameters! (has %d)\n", c_callargs));
    result = str_cat2(result, str_printf("%scall BBgetint\n", c_pad));
  }
  else if (strcmp(name, "out_str") == 0) {
    if (c_callargs == 0)
      core_error(strdup("Error: out_str should have at least 1 parameter!\n"));
    result = str_cat2(result, str_printf("%scall BBoutstr\n", c_pad));
    result = str_cat2(result, str_printf("%scall BBFalse\n", c_pad));
  }
  else {
    addFunctionCall(name, c_callargs);
    char *tname = core_translate_name(name);
    result = str_cat2(result, str_printf("%scall fn%sStart\n", c_pad, tname));
    free(tname);
  }
  node_set(node, result);
  node_free(node->bottom);
  c_stack -= c_callargs - 1;
}

void core_call_args(TreeNode *node) {
  node_traverse(node->bottom);
  node_set(node, strdup(node->bottom->code));
  node_free(node->bottom);
}

void core_void_return(TreeNode *node) {
  node_traverse(node->bottom);
  char *result = strdup(node->bottom->code);
  result = str_cat2(result, str_printf("%scall BDestroy\n", c_pad));
  node_set(node, result);
  node_free(node->bottom);
  c_stack--;
}

void core_arit_binary(TreeNode *node) {
  node_traverse(node->bottom);
  node_traverse(node->bottom->next);
  char *result = strdup(node->bottom->code);
  result = str_cat(result, node->bottom->next->code);
  result = str_cat2(result, str_printf("%scall BB%s\n", c_pad, node->code));
  node_set(node, result);
  node_free(node->bottom->next);
  node_free(node->bottom);
  c_stack--;
}

void core_arit_unary(TreeNode *node) {
  node_traverse(node->bottom);
  char *result = strdup(node->bottom->code);
  result = str_cat2(result, str_printf("%scall BB%s\n", c_pad, node->code));
  node_set(node, result);
  node_free(node->bottom);
}

void core_set_variable(TreeNode *node) {
  node_traverse(node->bottom);
  char *blob = core_translate_var(node->bottom->next->code);
  char *result = strdup(node->bottom->code);
  result = str_cat2(result, str_printf("%seax = %s + 0\n", c_pad, blob));
  result = str_cat2(result, str_printf("%scall BDestroyR\n", c_pad));
  result = str_cat2(result, str_printf("%stop %s\n", c_pad, blob));
  result = str_cat2(result, str_printf("%spop\n", c_pad));
  node_set(node, result);
  free(blob);
  node_free(node->bottom->next);
  node_free(node->bottom);
  c_stack--;
}

void core_get_length(TreeNode *node) {
  node_traverse(node->bottom);
  char *result = strdup(node->bottom->code);
  result = str_cat2(result, str_printf("%scall BBGetLength\n", c_pad));
  node_set(node, result);
  node_free(node->bottom);
}

void core_set_length(TreeNode *node) {
  node_traverse(node->bottom);
  char *blob = core_translate_var(node->bottom->next->code);
  char *result = strdup(node->bottom->code);
  result = str_cat2(result, str_printf("%spush %s\n", c_pad, blob));
  result = str_cat2(result, str_printf("%scall BBSetLength\n", c_pad));
  result = str_cat2(result, str_printf("%stop %s\n", c_pad, blob));
  result = str_cat2(result, str_printf("%spop\n", c_pad));
  node_set(node, result);
  free(blob);
  node_free(node->bottom->next);
  node_free(node->bottom);
  c_stack--;
}

void core_get_element(TreeNode *node) {
  node_traverse(node->bottom);
  node_traverse(node->bottom->next);
  char *result = strdup(node->bottom->code);
  result = str_cat(result, node->bottom->next->code);
  result = str_cat2(result, str_printf("%scall BBGetElement\n", c_pad));
  node_set(node, result);
  node_free(node->bottom->next);
  node_free(node->bottom);
  c_stack--;
}

void core_set_element(TreeNode *node) {
  node_traverse(node->bottom);
  node_traverse(node->bottom->next);
  char *blob = core_translate_var(node->bottom->next->next->code);
  char *result = strdup(node->bottom->code);
  result = str_cat(result, node->bottom->next->code);
  result = str_cat2(result, str_printf("%spush %s\n", c_pad, blob));
  result = str_cat2(result, str_printf("%scall BBSetElement\n", c_pad));
  result = str_cat2(result, str_printf("%stop %s\n", c_pad, blob));
  result = str_cat2(result, str_printf("%spop\n", c_pad));
  node_set(node, result);
  free(blob);
  node_free(node->bottom->next->next);
  node_free(node->bottom->next);
  node_free(node->bottom);
  c_stack -= 2;
}

void core_line(TreeNode *node) {
  node_traverse(node->bottom);
  node_set(node, strdup(node->bottom->code));
  node_free(node->bottom);
}

void core_codeblock(TreeNode *node) {
  TreeNode *iterator = node->bottom;
  TreeNode *last = iterator->previous;
  node_traverse(iterator);
  char *result = strdup(iterator->code);
  while (iterator != last) {
    iterator = iterator->next;
    node_free(iterator->previous);
    node_traverse(iterator);
    result = str_cat(result, iterator->code);
  }
  node_set(node, result);
  node_free(iterator);
}

void core_initialize() {
  c_error = strdup("");
  core_pad_reset();
  root = node_new();
  node_bottom(root, node_new());
  nodefunc[NODE_EMPTY] = core_empty;
  nodefunc[NODE_ROOT] = NULL;
  nodefunc[NODE_FUNCTION] = core_function;
  nodefunc[NODE_FUNC_ARGS] = core_func_args;
  nodefunc[NODE_RETURN] = core_return;
  nodefunc[NODE_IF] = core_if;
  nodefunc[NODE_WHILE] = core_while;
  nodefunc[NODE_PUSH_VAR] = core_push_var;
  nodefunc[NODE_PUSH_BOOL] = core_push_bool;
  nodefunc[NODE_PUSH_DEC] = core_push_dec;
  nodefunc[NODE_PUSH_HEX] = core_push_hex;
  nodefunc[NODE_PUSH_STR] = core_push_str;
  nodefunc[NODE_CALLFN] = core_callfn;
  nodefunc[NODE_CALL_ARGS] = core_call_args;
  nodefunc[NODE_VOID_RETURN] = core_void_return;
  nodefunc[NODE_ARIT_BINARY] = core_arit_binary;
  nodefunc[NODE_ARIT_UNARY] = core_arit_unary;
  nodefunc[NODE_SET_VARIABLE] = core_set_variable;
  nodefunc[NODE_GET_LENGTH] = core_get_length;
  nodefunc[NODE_SET_LENGTH] = core_set_length;
  nodefunc[NODE_GET_ELEMENT] = core_get_element;
  nodefunc[NODE_SET_ELEMENT] = core_set_element;
  nodefunc[NODE_LINE] = core_line;
  nodefunc[NODE_CODEBLOCK] = core_codeblock;
  addFunction("get_chr", 0);
  addFunction("get_int", 0);
  addFunction("out_str", 0);
}

void core_finalize() {
  clearLocals();
  clearGlobals();
  clearFunctions();
}

void root_output() {
  asm_print(c_output);
  TreeNode *iterator = root;
  TreeNode *last = root->previous;
  node_traverse(iterator->bottom);
  node_free(iterator->bottom);
  while (iterator != last) {
    iterator = iterator->next;
    node_free(iterator->previous);
    node_traverse(iterator->bottom);
    node_free(iterator->bottom);
  }
  node_free(iterator);
  int bob = varFunction("BoB");
  if (bob == -1)
    core_error(strdup("Error: Function BoB() not defined!\n"));
  if (bob > 0) {
    char *msg = strdup("Error: BoB() should have 0 parameters!");
    msg = str_cat2(msg, str_printf(" (has %d)\n", bob));
    core_error(msg);
  }
  char **names;
  int count, i;
  varInvalidFunctionCalls(&names, &count);
  for (i = 0; i < count; i++) {
    core_error(str_printf("Error: Incorrect call to function \"%s\"!\n", names[i]));
  }
  i = 0;
  char *global = varGlobalNo(i);
  fputs("BoBInit:\n", c_output);
  while (global != NULL) {
    fputs("  call BBFalse\n", c_output);
    fprintf(c_output, "  top %s\n", global);
    fputs("  pop\n", c_output);
    i++;
    global = varGlobalNo(i);
  }
  fputs("  call fnBoBStart\n", c_output);
  fputs(c_error, stderr);
  free(c_error);
}

